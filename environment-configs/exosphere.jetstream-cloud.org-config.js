"use strict";

var config = {
  showDebugMsgs: false,
  cloudCorsProxyUrl: "https://exosphere.jetstream-cloud.org/proxy",
  urlPathPrefix: "exosphere",
  palette: {
    light: {
      primary: {
        r: 150,
        g: 35,
        b: 38,
      },
      secondary: {
        r: 0,
        g: 0,
        b: 0,
      },
    },
    dark: {
      primary: {
        r: 255,
        g: 70,
        b: 95,
      },
      secondary: {
        r: 0,
        g: 0,
        b: 0,
      },
    },
  },
  logo: "assets/img/jetstream1-logo-white.svg",
  favicon: "assets/img/jetstream1-favicon.ico",
  appTitle: "Exosphere for Jetstream Cloud",
  topBarShowAppTitle: false,
  defaultLoginView: "jetstream1",
  aboutAppMarkdown:
    "This is the Exosphere interface for [Jetstream Cloud](https://jetstream-cloud.org), currently in beta. If you require assistance, please email help@jetstream-cloud.org and specify you are using Exosphere.\\\n\\\nUse of this site is subject to the Exosphere hosted sites [Privacy Policy](https://gitlab.com/exosphere/exosphere/-/blob/master/docs/privacy-policy.md) and [Acceptable Use Policy](https://gitlab.com/exosphere/exosphere/-/blob/master/docs/acceptable-use-policy.md).",
  supportInfoMarkdown:
    "Please read about [using instances](https://iujetstream.atlassian.net/wiki/display/JWT/Jetstream+Public+Wiki) or [troubleshooting instances](https://wiki.jetstream-cloud.org/Troubleshooting+and+FAQ) for answers to common problems before submitting a request to support staff.",
  userSupportEmail: "help@jetstream-cloud.org",
  openIdConnectLoginConfig: {
    keystoneAuthUrl: "https://js2.jetstream-cloud.org:5000/identity/v3",
    webssoKeystoneEndpoint:
      "/auth/OS-FEDERATION/websso/openid?origin=https://exosphere.jetstream-cloud.org/exosphere/oidc-redirector",
    oidcLoginIcon: "assets/img/XSEDE_Logo_Black_INF.png",
    oidcLoginButtonLabel: "Add XSEDE Account",
    oidcLoginButtonDescription: "Jetstream2 only",
  },
  localization: {
    openstackWithOwnKeystone: "cloud",
    openstackSharingKeystoneWithAnother: "region",
    unitOfTenancy: "allocation",
    maxResourcesPerProject: "quota",
    pkiPublicKeyForSsh: "SSH public key",
    virtualComputer: "instance",
    virtualComputerHardwareConfig: "flavor",
    cloudInitData: "boot script",
    commandDrivenTextInterface: "web shell",
    staticRepresentationOfBlockDeviceContents: "image",
    blockDevice: "volume",
    nonFloatingIpAddress: "internal IP address",
    floatingIpAddress: "public IP address",
    publiclyRoutableIpAddress: "public IP address",
    graphicalDesktopEnvironment: "web desktop",
  },
  instanceConfigMgtRepoUrl: null,
  instanceConfigMgtRepoCheckout: null,
  sentryConfig: {
    dsnPublicKey: "2c1487a758db4414b30ea690ab46b338",
    dsnHost: "o1143942.ingest.sentry.io",
    dsnProjectId: "6205105",
    releaseVersion: "latest",
    environmentName: "exosphere.jetstream-cloud.org",
  },
};

/* Matomo tracking code */
var _paq = (window._paq = window._paq || []);
/* tracker methods like "setCustomDimension" should be called before "trackPageView" */
_paq.push(["trackPageView"]);
_paq.push(["enableLinkTracking"]);
(function () {
  var u = "//matomo.exosphere.app/";
  _paq.push(["setTrackerUrl", u + "matomo.php"]);
  _paq.push(["setSiteId", "3"]);
  var d = document,
    g = d.createElement("script"),
    s = d.getElementsByTagName("script")[0];
  g.type = "text/javascript";
  g.async = true;
  g.src = u + "matomo.js";
  s.parentNode.insertBefore(g, s);
})();
